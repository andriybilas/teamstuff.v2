import { LayoutComponent } from '../layout/layout.component';
import { LoginComponent } from '../views/login.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' }
        ]
    },

    {path: 'login', component: LoginComponent},

    // Not found
    { path: '**', redirectTo: 'home' }

];
