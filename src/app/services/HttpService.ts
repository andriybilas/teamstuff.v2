import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


@Injectable()
export class HttpService {
    options = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

    constructor(private http: HttpClient, private router: Router) {

    }

    get<T>(url: string): Observable<T> {
        return this.http.get<T>(`/api/${url}`, this.options)
            .pipe(catchError(this.handleError<T>('api:get' + url)));
    }

    post(url: string, model): Observable<any> {
        return this.http.post(`/api/${url}`, model, this.options)
            .pipe(catchError(this.handleError('api:get' + url)));
    }

    private handleError<T>(operations = 'operation') {
        return (error: any): Observable<T> => {
            if (error.status === 403) {
                this.router.navigate(['/login']);
            }
            if (error.status === 401) {
                this.router.navigate(['/login']);
            }

            return Observable.throw(error);
        };
    }

}
