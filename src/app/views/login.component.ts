import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../services/HttpService';

@Component({
    selector: 'login-form',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    constructor(private router: Router, private http: HttpService) {

    }

    loginForm: FormGroup;
    result: string;

    login($event, loginForm) {
        loginForm.submitted = true;

        if (!loginForm.valid) {
            return;
        }

        const model = {
            userName: loginForm.value.userName,
            password: loginForm.value.password,
            persistent: loginForm.value.persistent
        };

        this.http.post('/authorization/signin', model)
            .subscribe(res => {
                this.result = res;
            });
    }

    register() {
        this.http.get('authorization/dummy').subscribe((res: string) => {
            this.result = res;
        });
    }

    logout() {
        this.http.get('authorization/logout').subscribe(() => {
            this.router.navigate(['/login']);
        });
    }

    ngOnInit(): void {
        this.loginForm = new FormGroup({
            userName: new FormControl(),
            password: new FormControl(),
            persistent: new FormControl()
        });
    }
}
